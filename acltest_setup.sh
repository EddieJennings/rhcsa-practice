#!/bin/bash

echo "##############################################################"
echo "This script will create a test environment for practicing ACLs"
echo "##############################################################"

LOGFILE="/tmp/rhcsa-practice.log"
echo "Creating logfile." | tee $LOGFILE

echo -e "\nChecking user privileges.\n"

if [ $(id -u) -ne $(id -u root) ]
	then
		echo "You are not root!" | tee -a $LOGFILE
		echo "Please re-run the script either as root or using sudo." | tee -a $LOGFILE
		exit 1
fi

# Eventually, I want to make this into a loop, where you choose the number
# of users to create

echo "Let's create two user accounts."
echo "Enter the first username:"
read USER1
echo "Is $USER1 the correct name?  Enter Y or N."
read USER1CONFIRM

while [ "$USER1CONFIRM" != "Y" -a "$USER1CONFIRM" != "y" ] 
	do
		echo "Enter the first username:"
		read USER1
		echo "Is $USER1 the correct name? Enter Y or N."
		read USER1CONFIRM
	done

echo "Enter a password for $USER1"
read USER1PASS
echo "Confirm the password for $USER1"
read USER1PASSCONFIRM

while [ "$USER1PASS" != "$USER1PASSCONFIRM" ]
	do
		echo "Passwords do not match!"
		echo "Enter a password for $USER1"
		read USER1PASS
		echo "Confirm the password for $USER1"
		read USER1PASSCONFIRM
	done

echo "Creating the $USER1 with the password $USER1PASS" | tee -a $LOGFILE
useradd $USER1
echo $USER1PASS | passwd --stdin $USER1 >> /dev/null

echo "Enter the second username:"
read USER2
echo "Is $USER2 the correct name?  Enter Y or N."
read USER2CONFIRM

while [ "$USER2CONFIRM" != "Y" -a "$USER2CONFIRM" != "y" ]
        do
                echo "Enter the first username:"
                read USER2
                echo "Is $USER2 the correct name? Enter Y or N."
                read USER2CONFIRM
        done

echo "Enter a password for $USER2"
read USER2PASS
echo "Confirm the password for $USER2"
read USER2PASSCONFIRM

while [ "$USER2PASS" != "$USER2PASSCONFIRM" ]
        do
                echo "Passwords do not match!"
                echo "Enter a password for $USER2"
                read USER2PASS
                echo "Confirm the password for $USER2"
                read USER2PASSCONFIRM
        done


echo "Creating the $USER2 with the password $USER2PASS" | tee -a $LOGFILE
useradd $USER2
echo "$USER2PASS" | passwd --stdin $USER2 >> /dev/null

####
# Begin Tasks for Groups
####

echo -e "We will now create two groups.\nType the name of the first group, and press enter."
read GROUP1
echo "Type the name of the second group, and press enter."
read GROUP2
echo -e "You entered $GROUP1 and $GROUP2.\nIs this correct?  Enter Y or N."
read GROUPCONFIRM

while [ "$GROUPCONFIRM" != "Y" -a "$GROUPCONFIRM" != "y" ]
        do
        	echo "Type the name of the first group, and press enter."
		read GROUP1
		echo "Type the name of the second group, and press enter."
		read GROUP2
		echo -e "You entered $GROUP1 and $GROUP2.\nIs this correct?  Enter Y or N."
		read GROUPCONFIRM

	done

echo "Creating groups $GROUP1 and $GROUP2" | tee -a $LOGFILE
groupadd $GROUP1
groupadd $GROUP2

echo "Next we will place users $USER1 and $USER2 each into one group."
echo "To get the most benefit do not place $USER1 and $USER2 in the same grouop."

echo "Which user will go into the $GROUP1 group?  Type the appropriate number, and press enter."
echo -e "1) $USER1\n2) $USER2"
read GROUPCHOICE

while [ "$GROUPCHOICE" -lt 1 -o "$GROUPCHOICE" -gt 2 ]
	do
		echo "Invalid choice."
		echo "Which user will go into the $GROUP1 group?  Type the appropriate number, and press enter."
		echo -e "1) $USER1\n2) $USER2"
		read GROUPCHOICE
	done

if [ "$GROUPCHOICE" -eq 1 ]
	then
		usermod -aG $GROUP1 $USER1
	else
		usermod -aG $GROUP1 $USER2
fi

echo "Which user will go into the $GROUP2 group?  Type the appropriate number, and press enter."
echo -e "1) $USER1\n2) $USER2"
read GROUPCHOICE

while [ "$GROUPCHOICE" -lt 1 -o "$GROUPCHOICE" -gt 2 ]
	do
		echo "Invalid choice."
		echo "Which user will go into the $GROUP2 group?  Type the appropriate number, and press enter."
		echo -e "1) $USER1\n2) $USER2"
		read GROUPCHOICE
	done

if [ "$GROUPCHOICE" -eq 1 ]
	then
		usermod -aG $GROUP2 $USER1
	else
		usermod -aG $GROUP2 $USER2
fi

echo "Group memberships have been applied!"
id $USER1 >> $LOGFILE
id $USER2 >> $LOGFILE

####
# Create test directories and files
####

echo "Next, we will create some test directories and files within /rhcsa_test"

if [ -d /rhcsa_test ]
	then
		echo "/rhcsa_test already exists!"
		echo "Enter the name for the test directory"
		read TESTDIRINPUT
		TESTDIRNAME="$TESTDIRINPUT"
		TESTDIR="/$TESTDIRNAME"
		mkdir $TESTDIR
	else
		TESTDIRNAME="rhcsa_test"
		TESTDIR="/$TESTDIRNAME"
		mkdir $TESTDIR
fi

echo "Creating directories for the $GROUP1 group."
mkdir -p $TESTDIR/$GROUP1/projects_public $TESTDIR/$GROUP1/projects_private

echo "Creating directories for the $GROUP2 group."
mkdir -p $TESTDIR/$GROUP2/projects_public $TESTDIR/$GROUP2/projects_private

echo "Creating test files."
echo "Details for $GROUP1's public projects." > $TESTDIR/$GROUP1/projects_public/publicprojects.txt
echo "Details for $GROUP1's private projects." > $TESTDIR/$GROUP1/projects_private/privateprojects.txt
echo "Details for $GROUP2's public projects." > $TESTDIR/$GROUP2/projects_public/publicprojects.txt
echo "Details for $GROUP2's private projects." > $TESTDIR/$GROUP2/projects_private/privateprojects.txt

echo "Setting file ownership and permissions."
chown root:$GROUP1 -R $TESTDIR/$GROUP1
chown root:$GROUP2 -R $TESTDIR/$GROUP2

chmod 770 $(find $TESTDIR -type d -group $GROUP1)
chmod 770 $(find $TESTDIR -type d -group $GROUP2)
chmod 660 $(find $TESTDIR -type f -group $GROUP1)
chmod 660 $(find $TESTDIR -type f -group $GROUP2)

ls -l / | grep $TESTDIRNAME >> $LOGFILE
ls -lR $TESTDIR >> $LOGFILE

echo "All done!  Enjoy practicing with access control lists!"
echo "Review the $LOGFILE logfile to see what was created."
